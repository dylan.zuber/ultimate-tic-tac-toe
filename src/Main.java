import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse the standard input
 * according to the problem statement.
 **/
class Player {
    static int US = 1;
    static int THEM = 2;

    public static void main(final String args[]) {
        final Scanner in = new Scanner(System.in);

        final Agent agent = new Randy();
        final Map<Cell, Integer> occupiedCells = new HashMap<Cell, Integer>();
        // game loop        while (true) {
            final Cell opponentMove = new Cell(in.nextInt(), in.nextInt());
            if (opponentMove.row >= 0 && opponentMove.col >= 0) {
                occupiedCells.put(opponentMove, THEM);
                System.err.printf("Opponent move: %s", opponentMove);
            }
            final int validActionCount = in.nextInt();
            System.err.printf("%d valid actions: \n", validActionCount);
            final List<Cell> validActions = new ArrayList<Cell>();
            for (int i = 0; i < validActionCount; i++) {
                final Cell action = new Cell(in.nextInt(), in.nextInt());
                System.err.println(action);
                validActions.add(action);
            }

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");
            Cell choice = Cell.ORIGIN;
            if (validActions.size() > 0)
                choice = agent.chooseAction(occupiedCells, validActions);

            occupiedCells.put(choice, US);
            System.out.println(choice);
        }
    }
}

interface Agent {
    Cell chooseAction(Map<Cell, Integer> occupiedCells, List<Cell> validActions);
}

class Randy implements Agent {
    final Random r;

    public Randy(Random r) {
        this.r = r;
    }

    public Randy() {
        this(new Random());
    }

    public Cell chooseAction(Map<Cell, Integer> occupiedCells, List<Cell> validActions) {
        return validActions.get(r.nextInt(validActions.size()));
    }
}

class Cell {
    public Cell(final int row, final int col) {
        this.row = row;
        this.col = col;
    }

    public final int row, col;

    @Override
    public String toString() {
        return String.format("%d %d", row, col);
    }

    @Override
    public int hashCode() {
        return String.format("%d%d", row, col).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Cell))
            return false;

        Cell other = (Cell) obj;
        return row == other.row && col == other.col;

    }

    public static Cell ORIGIN = new Cell(0, 0);
}